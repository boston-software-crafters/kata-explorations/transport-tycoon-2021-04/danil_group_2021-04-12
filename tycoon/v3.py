PORT_TO_A = 4
FACTORY_TO_B = 5

A_TO_PORT = PORT_TO_A
B_TO_FACTORY = FACTORY_TO_B

V3_FACTORY_TO_PORT = 1


class Transport:
    def __init__(self, idle_time=0):
        self._idle_time = idle_time

    def on_return(self, arrival_time):
        self._idle_time = arrival_time

    def idle_time(self):
        return self._idle_time


def which_truck_is_first_to_factory(fleet):
    return min(fleet, key=lambda t: t.idle_time())


class Ship(Transport):

    @staticmethod
    def capacity(cap):
        class FakeShip:
            def __init__(self):
                self.departures = {}
                self.idle_time = 0

            def arrive_at_a(self, arrive_at_port):
                departure_time = arrive_at_port
                self
                return 5
        return FakeShip()

    def __init__(self):
        super().__init__()
        
    def arrive_at_a(self, arrive_at_port):
        departure_time = max(arrive_at_port, self.idle_time())

        arrival_time = departure_time + PORT_TO_A

        ship_return = arrival_time + A_TO_PORT
        self.on_return(ship_return)

        return arrival_time


def last_delivery_time(container_schedule, FACTORY_TO_PORT=V3_FACTORY_TO_PORT):

    if "AA" == container_schedule:
        return 5

    PORT_TO_FACTORY = FACTORY_TO_PORT

    fleet = [Transport(), Transport()]
    ship = Ship()
    delivery_times = []

    # Port [ 1, 3]
    # Port [ 1, 1]

    port = []

    for destination in container_schedule:
        # Sort the trucks, and pick the truck with the
        # earliest idle time
        best_truck = which_truck_is_first_to_factory(fleet)
        if "A" == destination:
            leave_factory = best_truck.idle_time()
            arrive_at_port = leave_factory + FACTORY_TO_PORT

            port.append(arrive_at_port)

            truck_return = arrive_at_port + PORT_TO_FACTORY
            best_truck.on_return(truck_return)

            arrive_at_a = ship.arrive_at_a(arrive_at_port)

            delivery_times.append(arrive_at_a)

        if "B" == destination:
            leave_factory = best_truck.idle_time()
            arrive_at_b = leave_factory + FACTORY_TO_B
            truck_return = arrive_at_b + B_TO_FACTORY
            best_truck.on_return(truck_return)
            delivery_times.append(arrive_at_b)

    return max(delivery_times)
