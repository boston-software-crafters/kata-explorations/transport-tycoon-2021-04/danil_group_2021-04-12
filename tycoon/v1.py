import tycoon.v2


def which_truck_is_first_to_factory(fleet):
    return min(fleet, key=lambda t: t.idle_time())


def last_delivery_time(container_schedule):
    V1_FACTORY_TO_PORT = 1
    return tycoon.v2.last_delivery_time(container_schedule,  FACTORY_TO_PORT=V1_FACTORY_TO_PORT)
