from tycoon.v1 import last_delivery_time
import unittest


class TruckOrderTest(unittest.TestCase):
    def test_first_truck_back_to_factory(self):
        from tycoon.v2 import Transport, which_truck_is_first_to_factory

        truck_1 = Transport(2)
        truck_2 = Transport(5)

        best_truck = which_truck_is_first_to_factory([truck_1, truck_2])

        assert truck_1 == best_truck
