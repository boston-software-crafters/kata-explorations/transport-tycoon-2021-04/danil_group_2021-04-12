from tycoon.v2 import last_delivery_time
import unittest


class LastDeliveryTimeTest(unittest.TestCase):
    def test_schedule(self):
        schedules = [(6, "A"),
                     (14, "AA"),
                     (14, "AAB"), ]

        for expected_time, container_schedule in schedules:
            with self.subTest(container_schedule):
                self.assertEqual(
                    expected_time,
                    last_delivery_time(
                        container_schedule
                    ),
                    "container schedule === %s" % container_schedule
                )
