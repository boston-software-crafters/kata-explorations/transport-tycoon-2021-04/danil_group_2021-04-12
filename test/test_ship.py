import unittest
from tycoon.v3 import Ship


class ShipTest(unittest.TestCase):

    def test_ship_1(self):
        ship = Ship()
        time1 = ship.arrive_at_a(1)
        assert time1 == 5
        time2 = ship.arrive_at_a(1)
        assert time2 == 13

    def test_ship_2(self):
        ship = Ship.capacity(2)
        time1 = ship.arrive_at_a(1)
        assert time1 == 5
        time2 = ship.arrive_at_a(1)
        assert time2 == 5
